<?php 

	# Include the Dropbox SDK libraries
	require_once "lib/Dropbox/autoload.php";
	use \Dropbox as dbx;
	$appInfo = dbx\AppInfo::loadFromJsonFile("app-info.json");
	$dbxClient = new dbx\Client("NrK0ObstJIcAAAAAAAAO8CYEk2rF9no2bWV1pVAmD7fag9JtahMgIT5Q2y5-PifM", "PHP-Example/1.0");
	$folderMetadata =null;

	$values= array();

	$getURLVar = str_replace("?","",strrchr($_SERVER['HTTP_REFERER'],"?"));
    $getURLVar = str_replace("&","=",$getURLVar);
    $getURLVar = str_getcsv($getURLVar,"=");


    //print_r($getURLVar);
   
    $i=0;
    foreach ($getURLVar as $value)
	{
		$values[$i] = $value;		
		$i++;
	} 
	
	

	//echo $values[1];
	//echo $values[3];
	


		

	


?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap-theme.css">
		<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap-theme.min.css">
		<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap.css">
	</head>

	<body>
		<div class="panel panel-success" id="success">
      		<div class="panel-heading" style="background-color:#006600;">
        		<h3 class="panel-title" id="panel-title">Documentos almacenados<a class="anchorjs-link" href="#panel-title"><span class="anchorjs-icon"></span></a></h3>
      		</div>
      		<div class="panel-body">
       			<div class="container bs-docs-container">
					<div class="row">
						<div class="bs-example" data-example-id="striped-table">
				    		<table class="table table-striped" id="records_table" style="font-size::;px;">
				      			<thead>
				        			<tr>
				     					<th>Nombre de Documento</th>
				     					<th>Tamano</th>
				    				</tr>
				      			</thead>
				      			<tbody>
				      				
				      				<!--<?php $a = 0; foreach ($folderMetadata["contents"] as $value) { ?>
				      				<tr>
				      					<th>1</th>
				      					<td><a id="download" tilllimit="<?php print_r($value['path']) ?>" href="#"><?php print_r($value['path']) ?></a></td>
				      					<td><?php print_r($value['size']) ?></td>
				      					<td><a class="delete" tilllimit="<?php print_r($value['path']) ?>" client="<?php $dbxClient ?>" href="#"><img src="https://cdn0.iconfinder.com/data/icons/30_Free_Black_ToolBar_Icons/20/Black_Trash.png"/></a></td>
				      				</tr>
				      				<?php $a++;} ?>-->
								</tbody>
				     			
				    		</table>
							
							<span class="btn btn-default btn-file" style="width:100%;">
								<input type="hidden" dropbox-path="<?php print_r($folderMetadata['path'])?>" id="my_hidden"/>
								
								<form id="sendinfo" action="" method="post" enctype="multipart/form-data">			
							    	<input type="file" name="fileToUpload" id="fileToUpload">
							    	<input type="submit" value="Cargar Archivo" id="uploadimage" class="btn btn-primary">
								</form>   

							</span>

							
				
				  		</div>
			  		</div>
  				</div>		
      		</div>
    	</div>

    	<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
	        <div class="modal-header">
	            <h1>PROCESSING</h1>
	        </div>
	        <div class="modal-body">
	            <div class="progress progress-striped active">
	                <div class="bar" style="width: 100%;"></div>
	            </div>
	        </div>
	    </div>
		
	</body>

	
	 <script src="./assets/js/jquery-1.11.2.min.js"></script>
	 <script src="./assets/js/bootstrap.js"></script>
	 <script src="./assets/js/bootstrap.min.js"></script>

	 <script>

	 	var file_name = '';
	 	var dropbox_path ='';
	 	var main_path =''

	 	$( document ).ready(function() {
			
			load_table();

		});
		
	 	/**
	 	 * [DELETE A DROPBOX FILE]
	 	 * @param  {[type]} )                  
	 	 */
		$('body').on('click', '.delete', function() {
			console.log('In delete function');
			var path_file = $(this).attr('current_path');
			

			var client ='';
			$('#pleaseWaitDialog').modal();
			
			$.ajax({
			    url: 'dropboxActions.php',
			    type: 'post',
			    data: { "path":path_file,"client":client},
			    success: function(response){
			    	console.log('Delete response:'+response); 
			    	$('#pleaseWaitDialog').modal('hide');
			    	load_table();
	    		},
	    		error:function(error){
	    			console.log(error);
	    		}
			});

			console.log(path_file);
		});

		/**
		 * [DOWNLOAD]
		 */
		$('body').on('click', '#download', function() {
			
			var dropbox_path2 ='/PortalOperaciones/DocumentosES/<?php print_r($values[1])?>/<?php print_r($values[3]) ?>';

			var fullPath = $(this).attr('ruta_completa');;
			console.log('fullpath'+fullPath);

			var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
			var filename = fullPath.substring(startIndex);
			if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
				filename = filename = filename.substring(1);
			}

			console.log('el nombre: '+filename)


			$.ajax({
			    url: 'dropboxActions.php',
			    type: 'post',
			    data: { "action":"download","filename":filename,"droxpox_path":fullPath},
			    success: function(response){
			    	 
			    	

			    	var myJsonString = JSON.stringify(response);
			    	console.log(response);
			    	window.open(response);

			    	$("success").load("index.php");
	    		},
	    		error:function(error){
	    			console.log(error);
	    		}
			});
		});
		

		
		/**
		 * [load_table description]
		 * @return {[type]} [description]
		 */
		function load_table(){
			$("#records_table tr").remove();	
			$.ajax({
			    url: 'dropboxActions.php',
			    type: 'post',
			    data: { "estacion":"<?php print_r($values[1])?>","folder":"<?php echo $values[3]?>"},
			    success: function(response){
			    	//console.log(response); 
			    	var parsed = JSON.parse(response);
			    	var trHTML ='';

			    	$.each(parsed, function(i, item) {
			    		trHTML += '<tr><td><a href="#" id="download" ruta_completa="'+item['path']+'">' + item['path'] + '</a></td><td>'+item['size']+'</td><td><a class="delete" current_path="'+item['path']+'" href="#"><image src="https://cdn0.iconfinder.com/data/icons/30_Free_Black_ToolBar_Icons/20/Black_Trash.png" /></a></td></tr>';
					});
			    	
			    	$('#records_table').append(trHTML);
			    	$('#pleaseWaitDialog').modal('hide');
			    	
	    		},
	    		error:function(error){
	    			console.log(error);
	    		}
			});
		}

		$( "#uploadimage" ).click(function() {
			dropbox_path = $(this).attr('dropbox-path');
		});

		/**
		 * [description]
		 * @param  {[type]} e)                   {			var                  pleaseWaitDiv [description]
		 * @param  {[type]} success:             function(response){					                              	console.log('Upload_response: '+response); 					    	pleaseWaitDiv.modal('hide');					    				    		} [description]
		 * @param  {[type]} error:function(error [description]
		 * @return {[type]}                      [description]
		 */
		$("#sendinfo").on('submit',(function(e) {
			var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
			pleaseWaitDiv.modal();
			e.preventDefault();
			
			$.ajax({
				url: "upload.php", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data){   // A function to be called if request succeeds
					var dropbox_path2 ='/PortalOperaciones/DocumentosES/<?php print_r($values[1])?>/<?php print_r($values[3]) ?>';

					$.ajax({
					    url: 'dropboxActions.php',
					    type: 'post',
					    data: { "upload":dropbox_path2,"relative_path":file_name,"filename":file_name},
					    success: function(response){
					    	console.log('Upload_response: '+response); 
					    	pleaseWaitDiv.modal('hide');
					    	load_table();
			    		},
			    		error:function(error){
			    			console.log(error);
			    		}
					});
				}
			});
		}));

		$('input[type=file]').change(function () {

			
			//var tmppath = URL.createObjectURL(event.target.files[0]);
			var fullPath = $(this).val();
			//console.log('NAME: '+$(this).val());

			var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
			var filename = fullPath.substring(startIndex);
			if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
				file_name = filename = filename.substring(1);
			}
			//file_to_upload = '/Users/emoran/Downloads/VIAJE-0221.pdf';
            //console.log(filename);
    	});
	 </script>

</html>
